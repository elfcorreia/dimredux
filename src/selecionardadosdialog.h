#ifndef SELECIONARDADOSDIALOG_H
#define SELECIONARDADOSDIALOG_H

#include <QDialog>
#include <QFile>
#include <QString>
#include <QStringList>
#include <QVector>

namespace Ui {
class SelecionarDadosDialog;
}

class SelecionarDadosDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SelecionarDadosDialog(QWidget *parent = 0);
    ~SelecionarDadosDialog();
    void loadFile(const QString &fileName);
    void setCurrentFile(const QString &fileName);

signals:
    void quantidadeDialog(int, bool);
    void listaNumerosDialog(QVector<float>, bool);

private slots:
    void on_rdArquivo_clicked(bool checked);
    void on_rdAleatorio_clicked(bool checked);
    void on_btnProcurar_clicked();
    void on_pushButton_clicked();

private:

    Ui::SelecionarDadosDialog *ui;
    QString curFile;
    QStringList lst_strValor;
    QVector<float> lst_numValor;
    int quantidade;

    void atualizarDialog();
    void lerDados(QFile *file);
};

#endif // SELECIONARDADOSDIALOG_H
