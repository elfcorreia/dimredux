#include "esfera.hpp"
#include <cmath>

Esfera::Esfera() {
	mRaio = 0;
}

Esfera::~Esfera() {

}

void Esfera::sphereFace(double passoRecursivo, GLdouble *a, GLdouble *b, GLdouble *c) {
    if(passoRecursivo > 1)
    {
       GLdouble d[3] = {a[0] + b[0], a[1] + b[1], a[2] + b[2]};
       GLdouble e[3] = {b[0] + c[0], b[1] + c[1], b[2] + c[2]};
       GLdouble f[3] = {c[0] + a[0], c[1] + a[1], c[2] + a[2]};

       normalize3(d);
       normalize3(e);
       normalize3(f);

       sphereFace(passoRecursivo-1, a, d, f);
       sphereFace(passoRecursivo-1, d, b, e);
       sphereFace(passoRecursivo-1, f, e, c);
       sphereFace(passoRecursivo-1, f, d, e);
    }

   glColor4ub(getColor().red(), getColor().green(), getColor().blue(), getColor().alpha());
   glBegin(GL_TRIANGLES);
      glNormal3dv(a);
      glVertex3d(a[0] * mRaio, a[1] * mRaio, a[2] * mRaio);
      glNormal3dv(b);
      glVertex3d(b[0] * mRaio, b[1] * mRaio, b[2] * mRaio);
      glNormal3dv(c);
      glVertex3d(c[0] * mRaio, c[1] * mRaio, c[2] * mRaio);
   glEnd();
}

void Esfera::normalize3(GLdouble *v) {
    GLdouble len = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    v[0] /= len;
    v[1] /= len;
    v[2] /= len;
}

void Esfera::onDraw() {
    GLdouble a[] = {1, 0, 0};
    GLdouble b[] = {0, 0, -1};
    GLdouble c[] = {-1, 0, 0};
    GLdouble d[] = {0, 0, 1};
    GLdouble e[] = {0, 1, 0};
    GLdouble f[] = {0, -1, 0};

    int recurse = 4;

    sphereFace(recurse, d, a, e);
    sphereFace(recurse, a, b, e);
    sphereFace(recurse, b, c, e);
    sphereFace(recurse, c, d, e);
    sphereFace(recurse, a, d, f);
    sphereFace(recurse, b, a, f);
    sphereFace(recurse, c, b, f);
    sphereFace(recurse, d, c, f);        
}