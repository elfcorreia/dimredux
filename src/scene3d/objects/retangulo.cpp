#include "retangulo.hpp"

Retangulo::Retangulo(double aX, double aY, double aZ, double bX, double bY, double bZ, double cX, double cY, double cZ, double dX, double dY, double dZ) {
    mAX = aX;
    mAY = aY;
    mAZ = aZ;
    mBX = bX;
    mBY = bY;
    mBZ = bZ;
    mCX = cX;
    mCY = cY;
    mCZ = cZ;
    mDX = dX;
    mDY = dY;
    mDZ = dZ;
}

Retangulo::~Retangulo() {}

void Retangulo::onDraw() {
    glColor3ub(getColor().red(), getColor().green(), getColor().blue());
    glBegin(GL_TRIANGLE_STRIP);                
        glVertex3f(mAX, mAY, mAZ);
        glVertex3f(mDX, mDY, mDZ);
        glVertex3f(mBX, mBY, mBZ);
        glVertex3f(mCX, mCY, mCZ);
        glVertex3f(mBX, mBY, mBZ);
        glVertex3f(mDX, mDY, mDZ);
        glVertex3f(mAX, mAY, mAZ);
    glEnd();
}