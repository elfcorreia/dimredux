#include "objeto3d.hpp"

Objeto3D::Objeto3D() {
    mLayoutX = mLayoutY = mLayoutZ =
    mTranslateX = mTranslateY = mTranslateZ =
    mRotationAngle = mRotationAxisX = mRotationAxisY = mRotationAxisZ = 0;
    mScaleX = mScaleY = mScaleZ = 1;
    mVisible = true;
}

Objeto3D::~Objeto3D() {

}

 void Objeto3D::draw() {
    if (!mVisible) {
        return;
    }

    glPushMatrix();
    glTranslated(mLayoutX + mTranslateX, mLayoutY + mTranslateY, mLayoutZ + mTranslateZ);
    glScaled(mScaleX, mScaleY, mScaleZ);
    onDraw();
    glPopMatrix();
}