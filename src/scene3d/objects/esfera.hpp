#ifndef ESFERA_HPP
#define ESFERA_HPP

#include <QOpenGLFunctions>
#include "objeto3d.hpp"

class Esfera: public Objeto3D {
private:
    double mRaio;
    void sphereFace(double passoRecursivo, GLdouble *a, GLdouble *b, GLdouble *c);
    void normalize3(GLdouble *v);
protected:
    void onDraw();    
public:
    Esfera();
    ~Esfera();

    inline double getRaio() const { return mRaio; }
    inline void setRaio(double value) { mRaio = value; }
};

#endif // ESFERA_HPP
