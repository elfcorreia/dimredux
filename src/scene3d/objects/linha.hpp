#ifndef LINHA_HPP
#define LINHA_HPP

#include <armadillo>
#include <QColor>
#include "objeto3d.hpp"

class Linha: public Objeto3D {
private:
    float mStartX;
    float mStartY;
    float mStartZ;
    float mEndX;
    float mEndY;
    float mEndZ;
    QColor mColor;
    float mWidth;
public:    
    Linha();
    Linha(float startX, float startY, float startZ, float endX, float endY, float endZ);
    Linha(const arma::rowvec &start, const arma::rowvec &end);
    virtual ~Linha();

    inline float getStartX() const { return mStartX; }
    inline float getStartY() const { return mStartY; }
    inline float getStartZ() const { return mStartZ; }
    inline float getEndX() const { return mEndX; }
    inline float getEndY() const { return mEndY; }
    inline float getEndZ() const { return mEndZ; }
    inline QColor getColor() const { return mColor; }
    inline float getWidth() const { return mWidth; }

    inline void setStartX(float value) { mStartX = value; }
    inline void setStartY(float value) { mStartY = value; }
    inline void setStartZ(float value) { mStartZ = value; }
    inline void setEndX(float value) { mEndX = value; }
    inline void setEndY(float value) { mEndY = value; }
    inline void setEndZ(float value) { mEndZ = value; }
    inline void setColor(QColor value) { mColor = value; }
    inline void setWidth(float value) { mWidth = value; }

    void onDraw();
};

#endif // LINHA_HPP
