#ifndef GRUPOOBJETOS3D_HPP
#define GRUPOOBJETOS3D_HPP

#include <vector>
#include "objeto3d.hpp"

class GrupoObjetos3D: public Objeto3D {
private:
    std::vector<Objeto3D*> mObjetos;
protected:
    void onDraw();
public:    
    GrupoObjetos3D();
    ~GrupoObjetos3D();

    void add(Objeto3D *objeto);
    std::vector<Objeto3D*> getObjetos();
};

#endif // GRUPOOBJETOS3D_HPP
