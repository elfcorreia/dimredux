#ifndef RETANGULO_HPP
#define RETANGULO_HPP

#include "objeto3d.hpp"

class Retangulo: public Objeto3D {
private:    
    double mAX;
    double mAY;
    double mAZ;
    double mBX;
    double mBY;
    double mBZ;
    double mCX;
    double mCY;
    double mCZ;
    double mDX;
    double mDY;
    double mDZ;
public:
    Retangulo(double aX, double aY, double aZ, double bX, double bY, double bZ, double cX, double cY, double cZ, double dX, double dY, double dZ);
    ~Retangulo();

    void onDraw();

};

#endif // RETANGULO_HPP
