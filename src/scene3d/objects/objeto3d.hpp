#ifndef OBJETO3D_HPP
#define OBJETO3D_HPP

#include <QOpenGLFunctions_3_3_Core>
#include <QColor>
#include <vector>

class Objeto3D {
private:
    bool mVisible;
    double mLayoutX;
    double mLayoutY;
    double mLayoutZ;
    double mTranslateX;
    double mTranslateY;
    double mTranslateZ;
    double mRotationAngle;
    double mRotationAxisX;
    double mRotationAxisY;
    double mRotationAxisZ;
    double mScaleX;
    double mScaleY;
    double mScaleZ;
    QColor mColor;
protected:
    Objeto3D(); 
public:
    virtual ~Objeto3D();

    inline bool isVisible() const { return mVisible; }
    inline double getLayoutX() const { return mLayoutX; }
    inline double getLayoutY() const { return mLayoutY; }
    inline double getLayoutZ() const { return mLayoutZ; }
    inline double getTranslateX() const { return mTranslateX; }
    inline double getTranslateY() const { return mTranslateY; }
    inline double getTranslateZ() const { return mTranslateZ; }
    inline double getRotationAngle() const { return mRotationAngle; }
    inline double getRotationAxisX() const { return mRotationAxisX; }
    inline double getRotationAxisY() const { return mRotationAxisY; }
    inline double getRotationAxisZ() const { return mRotationAxisZ; }
    inline double getScaleX() const { return mScaleX; }
    inline double getScaleY() const { return mScaleY; }
    inline double getScaleZ() const { return mScaleZ; }
    inline QColor getColor() const { return mColor; }

    inline void setVisible(bool value) { mVisible = value; }
    inline void setLayoutX(double value) { mLayoutX = value; }
    inline void setLayoutY(double value) { mLayoutY = value; }
    inline void setLayoutZ(double value) { mLayoutZ = value; }
    inline void setTranslateX(double value) { mTranslateX = value; }
    inline void setTranslateY(double value) { mTranslateY = value; }
    inline void setTranslateZ(double value) { mTranslateZ = value; }
    inline void setRotationAngle(double value) { mRotationAngle = value; }
    inline void setRotationAxisX(double value) { mRotationAxisX = value; }
    inline void setRotationAxisY(double value) { mRotationAxisY = value; }
    inline void setRotationAxisZ(double value) { mRotationAxisZ = value; }
    inline void setScaleX(double value) { mScaleX = value; }
    inline void setScaleY(double value) { mScaleY = value; }
    inline void setScaleZ(double value) { mScaleZ = value; }
    inline void setColor(QColor value) { mColor = value; }

    virtual void onDraw() = 0;
    void draw();
};

#endif // OBJETO3D_HPP
