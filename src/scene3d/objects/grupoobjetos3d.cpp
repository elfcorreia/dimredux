#include "grupoobjetos3d.hpp"

using namespace std;

GrupoObjetos3D::GrupoObjetos3D() {

}

GrupoObjetos3D::~GrupoObjetos3D() {

}

void GrupoObjetos3D::add(Objeto3D *objeto) {
    mObjetos.push_back(objeto);
}

vector<Objeto3D*> GrupoObjetos3D::getObjetos() {
    return mObjetos;
}

void GrupoObjetos3D::onDraw() {
    for (Objeto3D* obj: mObjetos) {            
        obj->draw();            
    }
}