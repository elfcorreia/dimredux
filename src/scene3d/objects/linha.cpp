#include "linha.hpp"

Linha::Linha() {

}

Linha::Linha(float startX, float startY, float startZ, float endX, float endY, float endZ) {
    mStartX = startX;
    mStartY = startY;
    mStartZ = startZ;
    mEndX = endX;
    mEndY = endY;
    mEndZ = endZ;
    mWidth = 1;
    mColor = Qt::black;
}

Linha::Linha(const arma::rowvec &start, const arma::rowvec &end) {
    mStartX = start.at(0);
    mStartY = start.at(1);
    mStartZ = start.at(2);
    mEndX = end.at(0);
    mEndY = end.at(1);
    mEndZ = end.at(2);
    mWidth = 1;
    mColor = Qt::black;
}

Linha::~Linha() {}

void Linha::onDraw() {
    glLineWidth(mWidth);
    glColor4ub(mColor.red(), mColor.green(), mColor.blue(), mColor.alpha());
    glBegin(GL_LINES);
        glVertex3f(mStartX, mStartY, mStartZ);
        glVertex3f(mEndX, mEndY, mEndZ);
    glEnd();
}