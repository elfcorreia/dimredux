#include "cena.hpp"
#include <iostream>

Cena::Cena(QWidget *parent) 
	: QOpenGLWidget(parent) {
    mEixoX = mEixoY = mEixoZ = 0;
    std::cout << "Cena constructor" << std::endl;
}

Cena::~Cena() {

}

void Cena::add(Objeto3D *objeto) {
    mObjetos.push_back(objeto);
}

void Cena::remove(Objeto3D *objeto) {
    mObjetos.erase(std::remove(mObjetos.begin(), mObjetos.end(), objeto), mObjetos.end());
}

void Cena::initializeGL() {
    glClearColor(255,255,255,255);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_EMISSION);

    // Create light components.
    GLfloat ambientLight[] = { 0.2f, 0.2f, 0.2f, 1.0f };
    GLfloat diffuseLight[] = { 0.8f, 0.8f, 0.8, 1.0f };
    GLfloat specularLight[] = { 0.5f, 0.5f, 0.5f, 1.0f };
    GLfloat position[] = { -10.0f, 10.0f, -10.0f, 1.0f };

    // Assign created components to GL_LIGHT0.
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT0, GL_POSITION, position);
}

void Cena::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0.0, 0.0, -20.0);
    glRotatef(mEixoX / 16.0, 1.0, 0.0, 0.0);
    glRotatef(mEixoY / 16.0, 0.0, 1.0, 0.0);
    glRotatef(mEixoZ / 16.0, 0.0, 0.0, 1.0);

    for (Objeto3D* obj: mObjetos) {
        obj->draw();
    }
    mOk = true;
}

void Cena::resizeGL(int width, int height) {
    int side = qMin(width, height);
    glViewport((width - side) / 2, 0, side, side);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-20, +20, -20, +20, 1.0, 60.0);
    //glFrustum(-20, +20, -20, +20, 1.0, 20.0);
    glMatrixMode(GL_MODELVIEW);
}

void Cena::run() {
    while (true) {
        emit repinte();
        QThread::msleep(25);
    }
}

void Cena::mousePressEvent(QMouseEvent *event) {
    mUltimaPosicao = event->pos();
}

void Cena::mouseMoveEvent(QMouseEvent *event) {
    int dx = event->x() - mUltimaPosicao.x();
    int dy = event->y() - mUltimaPosicao.y();

    if (event->buttons() & Qt::LeftButton) {
        setRotacaoX(mEixoX + 8 * dy);
        setRotacaoY(mEixoY + 8 * dx);
    } else if (event->buttons() & Qt::RightButton) {
        setRotacaoX(mEixoX + 8 * dy);
        setRotacaoZ(mEixoZ + 8 * dx);
    }

    mUltimaPosicao = event->pos();
}

void Cena::qNormalizeAngle(int &angle) {
    while (angle < 0)
           angle += 360 * 16;
       while (angle > 360)
           angle -= 360 * 16;
}

void Cena::setRotacaoX(int angulo) {
    qNormalizeAngle(angulo);
    if (angulo != mEixoX) {
        mEixoX = angulo;            
        update();
    }
}

void Cena::setRotacaoY(int angulo) {
    qNormalizeAngle(angulo);
    if (angulo != mEixoY) {
        mEixoY = angulo;
        //emit mudaRotacaoY(angulo);
        update();
    }
}

void Cena::setRotacaoZ(int angulo) {
    qNormalizeAngle(angulo);
    if (angulo != mEixoZ) {
        mEixoZ = angulo;
        //emit mudaRotacaoZ(angulo);
        update();
    }
}

void Cena::start() {
    connect(this, SIGNAL(repinte()), this, SLOT(update()));
    QThreadPool::globalInstance()->start(this);
}