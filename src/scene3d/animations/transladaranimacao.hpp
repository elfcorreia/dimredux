#ifndef TRANSLADARANIMACAO_HPP
#define TRANSLADARANIMACAO_HPP

#include <QRunnable>
#include <QThreadPool>
#include <iostream>
#include "animacao.hpp"
#include "../objects/objeto3d.hpp"

class TransladarAnimacao: public Animacao {
private:
    Objeto3D *mObjeto3D;
    double mOrigemX;
    double mOrigemY;
    double mOrigemZ;
    double mDestinoX;
    double mDestinoY;
    double mDestinoZ;
    double mVetorX;
    double mVetorY;
    double mVetorZ;
public:
    TransladarAnimacao(Objeto3D* objeto3D, double x, double y, double z);

    inline Objeto3D *getObjeto3D() const { return mObjeto3D; }
    inline double getOrigemX() const { return mOrigemX; }
    inline double getOrigemY() const { return mOrigemY; }
    inline double getOrigemZ() const { return mOrigemZ; }
    inline double getDestinoX() const { return mDestinoX; }
    inline double getDestinoY() const { return mDestinoY; }
    inline double getDestinoZ() const { return mDestinoZ; }
    inline double getVetorX() const { return mVetorX; }
    inline double getVetorY() const { return mVetorY; }
    inline double getVetorZ() const { return mVetorZ; }
    
    inline void setOrigemX(double value) { mOrigemX = value; }
    inline void setOrigemY(double value) { mOrigemY = value; }
    inline void setOrigemZ(double value) { mOrigemZ = value; }
    inline void setDestinoX(double value) { mDestinoX = value; }
    inline void setDestinoY(double value) { mDestinoY = value; }
    inline void setDestinoZ(double value) { mDestinoZ = value; }
    inline void setVetorX(double value) { mVetorX = value; }
    inline void setVetorY(double value) { mVetorY = value; }
    inline void setVetorZ(double value) { mVetorZ = value; }

    void virtual onStep(double relative_time);
    void virtual onFinished();
};

#endif // TRANSLADARANIMACAO_HPP
