#include "fadeoutanimacao.hpp"

FadeOutAnimacao::FadeOutAnimacao(Objeto3D* objeto3D)
	: Animacao() 
{
    mObjeto3D = objeto3D;
    mOriginalScaleX = objeto3D->getScaleX();
    mOriginalScaleY = objeto3D->getScaleY();
    mOriginalScaleZ = objeto3D->getScaleZ();
}

FadeOutAnimacao::~FadeOutAnimacao() {

}

void FadeOutAnimacao::onStep(double relative_time) {
    double complementar_relative_time = 1 - relative_time;
    mObjeto3D->setScaleX(mOriginalScaleX * complementar_relative_time);
    mObjeto3D->setScaleY(mOriginalScaleY * complementar_relative_time);
    mObjeto3D->setScaleZ(mOriginalScaleZ * complementar_relative_time);
}

void FadeOutAnimacao::onFinished() {
    mObjeto3D->setVisible(false);
    mObjeto3D->setScaleX(mOriginalScaleX);
    mObjeto3D->setScaleY(mOriginalScaleY);
    mObjeto3D->setScaleZ(mOriginalScaleZ);
}