#ifndef ANIMACAO_HPP
#define ANIMACAO_HPP

#include <vector>
#include <QRunnable>

class Animacao;

class AnimacaoListener {
public:
    virtual void onFinished(Animacao* animacao) = 0;
};

class Animacao: public QRunnable {
private:
    std::vector<AnimacaoListener*> mListeners;
    int mDuracao;
    double mDelay;
protected:
    virtual void onStep(double relative_time) = 0;
    virtual void onStart();
    virtual void onFinished();
    void notifyFinished();
    Animacao(); // instantiation only by subclass
public:
    virtual ~Animacao();

    inline int  getDuracao() const { return mDuracao; }
    inline double getDelay() const { return mDelay; }
    inline void setDuracao(int value) { mDuracao = value; }
    inline void setDelay(double value) { mDelay = value; }

    void run();

    void play();

    void addListener(AnimacaoListener *listener);

};


#endif // ANIMACAO_H
