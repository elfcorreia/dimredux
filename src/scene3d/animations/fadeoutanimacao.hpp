#ifndef FADEOUTANIMACAO_HPP
#define FADEOUTANIMACAO_HPP

#include <QRunnable>
#include <QThreadPool>
#include "animacao.hpp"
#include "../objects/objeto3d.hpp"

class FadeOutAnimacao: public Animacao {
private:
    Objeto3D *mObjeto3D;
    double mOriginalScaleX;
    double mOriginalScaleY;
    double mOriginalScaleZ;
protected:
    virtual void onStep(double relative_time);
    virtual void onFinished();
public:
    FadeOutAnimacao(Objeto3D* objeto3D);
    ~FadeOutAnimacao();
};

#endif // FADEOUTANIMACAO_H
