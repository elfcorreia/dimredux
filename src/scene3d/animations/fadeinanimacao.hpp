#ifndef FADEINANIMACAO_HPP
#define FADEINANIMACAO_HPP

#include <iostream>
#include "animacao.hpp"
#include "../objects/objeto3d.hpp"

class FadeInAnimacao: public Animacao {
private:
    Objeto3D *mObjeto3D;
    double mOriginalScaleX;
    double mOriginalScaleY;
    double mOriginalScaleZ;
protected:
    virtual void onStart();
    virtual void onStep(double relative_time);
public:
    FadeInAnimacao(Objeto3D* objeto3D);
};

#endif // FADEINANIMACAO_HPP
