#include "fadeinanimacao.hpp"

FadeInAnimacao::FadeInAnimacao(Objeto3D* objeto3D) 
    : Animacao() {
    mObjeto3D = objeto3D;
    mOriginalScaleX = objeto3D->getScaleX();
    mOriginalScaleY = objeto3D->getScaleY();
    mOriginalScaleZ = objeto3D->getScaleZ();
}

void FadeInAnimacao::onStart() {
    mObjeto3D->setScaleX(0);
    mObjeto3D->setScaleY(0);
    mObjeto3D->setScaleZ(0);
    mObjeto3D->setVisible(true);
}

void FadeInAnimacao::onStep(double relative_time) {
    mObjeto3D->setScaleX(mOriginalScaleX * relative_time);
    mObjeto3D->setScaleY(mOriginalScaleY * relative_time);
    mObjeto3D->setScaleZ(mOriginalScaleZ * relative_time);
}