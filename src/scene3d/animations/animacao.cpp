#include "animacao.hpp"
#include <QThread>
#include <QThreadPool>

Animacao::Animacao() {
	mDuracao = 1;
	mDelay = 0;
}

Animacao::~Animacao() {

}

void Animacao::onStart() {

}

void Animacao::onFinished() {

}

void Animacao::notifyFinished() {
    for (AnimacaoListener* l: mListeners) {
        l->onFinished(this);
    }
}

void Animacao::run() {
    onStart();
    QThread::msleep(mDelay);
    double time = 0;
    double quantum = 0.025 / mDuracao;
    while (time <= 1) {
        onStep(time);
        time += quantum;
        QThread::msleep(25);
    }
    onFinished();
    notifyFinished();
}

void Animacao::play() {
    QThreadPool::globalInstance()->start(this);
}

void Animacao::addListener(AnimacaoListener *listener) {
    mListeners.push_back(listener);
}
