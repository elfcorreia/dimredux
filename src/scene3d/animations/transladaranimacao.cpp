#include "transladaranimacao.hpp"

TransladarAnimacao::TransladarAnimacao(Objeto3D* objeto3D, double x, double y, double z)
	: Animacao() {
    mObjeto3D = objeto3D;
    mOrigemX = objeto3D->getLayoutX();
    mOrigemY = objeto3D->getLayoutY();
    mOrigemZ = objeto3D->getLayoutZ();
    mDestinoX = x;
    mDestinoY = y;
    mDestinoZ = z;
    mVetorX = mDestinoX - mOrigemX;
    mVetorY = mDestinoY - mOrigemY;
    mVetorZ = mDestinoZ - mOrigemZ;
}

void TransladarAnimacao::onStep(double relative_time) {
    mObjeto3D->setTranslateX(mVetorX * relative_time);
    mObjeto3D->setTranslateY(mVetorY * relative_time);
    mObjeto3D->setTranslateZ(mVetorZ * relative_time);
    //std::cout << mObjeto3D->getTranslateX() << " "
         //<< mObjeto3D->getTranslateY() << " "
         //<< mObjeto3D->getTranslateZ() << " "
         //<< std::endl;
}

void TransladarAnimacao::onFinished() {
    mObjeto3D->setTranslateX(0);
    mObjeto3D->setTranslateY(0);
    mObjeto3D->setTranslateZ(0);
    mObjeto3D->setLayoutX(mDestinoX);
    mObjeto3D->setLayoutY(mDestinoY);
    mObjeto3D->setLayoutZ(mDestinoZ);
}