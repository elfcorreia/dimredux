#ifndef CENA_HPP
#define CENA_HPP

#include <vector>
#include <QWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QColor>
#include <QWidget>
#include <QTime>
#include <QtEvents>
#include <QRunnable>
#include <QThread>
#include <QThreadPool>

#include "objects/objeto3d.hpp"

class Cena: public QOpenGLWidget, public QRunnable {
    Q_OBJECT
private:    
    int mEixoX;
    int mEixoY;
    int mEixoZ;
    bool mOk;
    QPoint mUltimaPosicao;
    std::vector<Objeto3D*> mObjetos;

    void qNormalizeAngle(int &angle);
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
public:
    void run();
    explicit Cena(QWidget *parent = 0);
    virtual ~Cena();

    void add(Objeto3D *objeto);
    void remove(Objeto3D *objeto);
signals: 
    void repinte();
public slots: 
    void setRotacaoX(int angulo);
    void setRotacaoY(int angulo);
    void setRotacaoZ(int angulo);
    void start();
};

#endif // CENA_HPP
