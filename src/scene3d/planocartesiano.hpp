#ifndef PLANOCARTESIANO_HPP
#define PLANOCARTESIANO_HPP

#include <QWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
// #include <mainwindow.h>]
#include <QMouseEvent>
#include <cmath>

class PlanoCartesiano : public QOpenGLWidget
{
    Q_OBJECT
public:
    explicit PlanoCartesiano(QWidget *parent = 0);
    ~PlanoCartesiano();

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

signals:
    void mudaRotacaoX(int angulo);
    void mudaRotacaoY(int angulo);
    void mudaRotacaoZ(int angulo);

public slots:
    void setRotacaoX(int angulo);
    void setRotacaoY(int angulo);
    void setRotacaoZ(int angulo);

private:
    void draw();

    int eixoX;
    int eixoY;
    int eixoZ;

    float matrix10x3[10][3];

    QPoint ultimaPosicao;

    void gerarMatrizAleatoria(int minimo, int maximo);

    void drawSphere(double radius, double x, double y, double z);
    void sphereFace(double recursive, double radius, GLdouble *a, GLdouble *b, GLdouble *c);
    inline void normalize3(GLdouble *v);

};

#endif // PLANOCARTESIANO_H
