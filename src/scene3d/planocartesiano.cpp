#include "planocartesiano.hpp"
// #include "mainwindow.h"
// #include "ui_mainwindow.h"
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QColor>
#include <QOpenGLWidget>
#include <QWidget>
#include <QTime>

PlanoCartesiano::PlanoCartesiano(QWidget *parent) : QOpenGLWidget(parent)
{
    eixoX = eixoY = eixoZ = 0;
    gerarMatrizAleatoria(0, 20);
}

PlanoCartesiano::~PlanoCartesiano()
{
}

void PlanoCartesiano::initializeGL()
{
    glClearColor(255,255,255,255);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);    
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    static GLfloat lightPosition[4] = { 0, 0, 10, 1.0 };
    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
}

void PlanoCartesiano::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0.0, 0.0, -10.0);
    glRotatef(eixoX / 16.0, 1.0, 0.0, 0.0);
    glRotatef(eixoY / 16.0, 0.0, 1.0, 0.0);
    glRotatef(eixoZ / 16.0, 0.0, 0.0, 1.0);
    draw();

}

void PlanoCartesiano::resizeGL(int width, int height)
{
    int side = qMin(width, height);
    glViewport(0, 0, side, side);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
#ifdef QT_OPENGL_ES_1
    glOrthof(-2, +2, -2, +2, 1.0, 15.0);
#else
    //glOrtho(-5, 20, -5, 20, 1.0, 30.0)
    glOrtho(-2, +2, -2, +2, 1.0, 20.0);
#endif
    glMatrixMode(GL_MODELVIEW);
}

void PlanoCartesiano::mousePressEvent(QMouseEvent *event)
{
    ultimaPosicao = event->pos();
}

void PlanoCartesiano::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - ultimaPosicao.x();
    int dy = event->y() - ultimaPosicao.y();

    if (event->buttons() & Qt::LeftButton) {
        setRotacaoX(eixoX + 8 * dy);
        setRotacaoY(eixoY + 8 * dx);
    } else if (event->buttons() & Qt::RightButton) {
        setRotacaoX(eixoX + 8 * dy);
        setRotacaoZ(eixoZ + 8 * dx);
    }

    ultimaPosicao = event->pos();
}

static void qNormalizeAngle(int &angle)
{
    while (angle < 0)
           angle += 360 * 16;
       while (angle > 360)
           angle -= 360 * 16;
}

void PlanoCartesiano::setRotacaoX(int angulo)
{
    qNormalizeAngle(angulo);
        if (angulo != eixoX) {
            eixoX = angulo;
            emit mudaRotacaoX(angulo);
            update();
        }

}

void PlanoCartesiano::setRotacaoY(int angulo)
{
    qNormalizeAngle(angulo);
        if (angulo != eixoY) {
            eixoY = angulo;
            emit mudaRotacaoY(angulo);
            update();
        }
}

void PlanoCartesiano::setRotacaoZ(int angulo)
{
    qNormalizeAngle(angulo);
        if (angulo != eixoZ) {
            eixoZ = angulo;
            emit mudaRotacaoZ(angulo);
            update();
        }
}


void PlanoCartesiano::draw()
{
    glColor3f(1.0, 0.0, 0.0);
    glLineWidth(1.0);
    //eixo X
    glBegin(GL_LINES);
        glVertex3f(-1,0,0);
        glVertex3f(1,0,0);
    glEnd();

    //eixo Y
    glBegin(GL_LINES);
        glVertex3f(0,-1,0);
        glVertex3f(0,1,0);
    glEnd();

    //eixo Z
    glBegin(GL_LINES);
        glVertex3f(0,0,-1);
        glVertex3f(0,0,1);
    glEnd();

    glPointSize(3.0);            

    //Points
    //glBegin(GL_POINTS);
        for(int i = 0; i < 10; i++ ){
            drawSphere(0.5, matrix10x3[i][0], matrix10x3[i][1],matrix10x3[i][2]);
            //glVertex3f(matrix10x3[i][0], matrix10x3[i][1],matrix10x3[i][2]);
        }
    //glEnd();
}

void PlanoCartesiano::gerarMatrizAleatoria(int minimo, int maximo)
{
    QTime time = QTime::currentTime();
    qsrand((uint)time.msec());
    for(int i = 0; i < 10; i++ ){
        for(int j=0; j < 3; j++){
           matrix10x3[i][j] = (qrand() % ((maximo + 1) - minimo) + minimo);
           matrix10x3[i][j] = matrix10x3[i][j] / 20;
        }

    }
}

void PlanoCartesiano::drawSphere(double radius, double x, double y, double z)
{
    GLdouble a[] = {1, 0, 0};
    GLdouble b[] = {0, 0, -1};
    GLdouble c[] = {-1, 0, 0};
    GLdouble d[] = {0, 0, 1};
    GLdouble e[] = {0, 1, 0};
    GLdouble f[] = {0, -1, 0};

    int recurse = 4;

    glPushMatrix();
    glTranslatef(x, y, z);
    sphereFace(recurse, radius, d, a, e);
    sphereFace(recurse, radius, a, b, e);
    sphereFace(recurse, radius, b, c, e);
    sphereFace(recurse, radius, c, d, e);
    sphereFace(recurse, radius, a, d, f);
    sphereFace(recurse, radius, b, a, f);
    sphereFace(recurse, radius, c, b, f);
    sphereFace(recurse, radius, d, c, f);
    glPopMatrix();
}

inline void PlanoCartesiano::normalize3(GLdouble *v)
{
   GLdouble len = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
   v[0] /= len;
   v[1] /= len;
   v[2] /= len;
}

void PlanoCartesiano::sphereFace(double passoRecursivo, double radius, GLdouble *a, GLdouble *b, GLdouble  *c)
{
    if(passoRecursivo > 1)
    {
       // Compute vectors halfway between the passed vectors
       GLdouble d[3] = {a[0] + b[0], a[1] + b[1], a[2] + b[2]};
       GLdouble e[3] = {b[0] + c[0], b[1] + c[1], b[2] + c[2]};
       GLdouble f[3] = {c[0] + a[0], c[1] + a[1], c[2] + a[2]};

       normalize3(d);
       normalize3(e);
       normalize3(f);

       sphereFace(passoRecursivo-1, radius, a, d, f);
       sphereFace(passoRecursivo-1, radius, d, b, e);
       sphereFace(passoRecursivo-1, radius, f, e, c);
       sphereFace(passoRecursivo-1, radius, f, d, e);
    }

   glBegin(GL_TRIANGLES);
      glNormal3dv(a);
      glVertex3d(a[0] * radius, a[1] * radius, a[2] * radius);
      glNormal3dv(b);
      glVertex3d(b[0] * radius, b[1] * radius, b[2] * radius);
      glNormal3dv(c);
      glVertex3d(c[0] * radius, c[1] * radius, c[2] * radius);
   glEnd();
}

