#ifndef TUTORIALDATA_H
#define TUTORIALDATA_H

#include <string>
#include <vector>
#include <armadillo>

#include <QVector>
#include <QDebug>

class TutorialData {
private:
    arma::mat mDadosOriginais;
    arma::rowvec mPontoMedio;
    arma::mat mDadosNormalizados;
    arma::mat mU;
    arma::mat mS;
    arma::mat mV;
    arma::colvec mAutovetor1;
    arma::colvec mAutovetor2;
    arma::colvec mAutovetor3;
    arma::mat mProjecaoNormalizadaPlano12;
    arma::mat mProjecaoNormalizadaPlano23;
    arma::mat mProjecaoNormalizadaPlano13;
    std::vector<arma::mat> mProjecoesNormalizadas;
    arma::mat mProjecaoSemMediaPlano12;
    arma::mat mProjecaoSemMediaPlano23;
    arma::mat mProjecaoSemMediaPlano13;
    std::vector<arma::mat> mProjecoesSemMedia;
    std::vector<arma::mat> mMatrizProjecoes;
    void helper_data();
public:
    TutorialData(QVector<float> lista, bool aleatorio=false);
    TutorialData(int quantidade=30, bool aleatorio=true);
    ~TutorialData();

    inline arma::mat getDadosOriginais() const { return mDadosOriginais; };
    inline arma::rowvec getPontoMedio() const { return mPontoMedio; };
    inline arma::mat getDadosNormalizados() const { return mDadosNormalizados; };
    inline arma::mat getU() const { return mU; };
    inline arma::mat getS() const { return mS; };
    inline arma::mat getV() const { return mV; };
    inline arma::colvec getAutovetor1() const { return mAutovetor1; };
    inline arma::colvec getAutovetor2() const { return mAutovetor2; };
    inline arma::colvec getAutovetor3() const { return mAutovetor3; };
    inline arma::mat getProjecaoNormalizadaPlano12() const { return mProjecaoNormalizadaPlano12; };
    inline arma::mat getProjecaoNormalizadaPlano23() const { return mProjecaoNormalizadaPlano23; };
    inline arma::mat getProjecaoNormalizadaPlano13() const { return mProjecaoNormalizadaPlano13; };
    inline std::vector<arma::mat> getProjecoesNormalizadas() const { return mProjecoesNormalizadas; };
    inline arma::mat getProjecaoSemMediaPlano12() const { return mProjecaoSemMediaPlano12; };
    inline arma::mat getProjecaoSemMediaPlano23() const { return mProjecaoSemMediaPlano23; };
    inline arma::mat getProjecaoSemMediaPlano13() const { return mProjecaoSemMediaPlano13; };
    inline std::vector<arma::mat> getProjecoesSemMedia() const { return mProjecoesSemMedia; };
    inline std::vector<arma::mat> getMatrizProjecoes() const { return mMatrizProjecoes; };
};

#endif // TUTORIALDATA_HPP
