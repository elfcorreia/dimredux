#include "tutorialdata.hpp"

using namespace std;
using namespace arma;

TutorialData::TutorialData(QVector<float> lista, bool aleatorio) {
    if(!aleatorio) {
        mat A;
        int linha = 0;
        int count = lista.count()-1;
        if (count <= 3) return; // o vetor precisa ter 4 valores, pois o ultimo e o zero

        if(count%3==0){
            for(int i = 0; i < count; i+=3){
                A << lista.value(i) << lista.value(i+1) << lista.value(i+2);
                mDadosOriginais.insert_rows(linha, A);
                linha++;
            }
            helper_data();
        }
    }
    else {
        return;
    }

}

TutorialData::TutorialData(int quantidade, bool aleatorio) {
    if (aleatorio) {
        mDadosOriginais = randu<mat>(quantidade, 3);
        mDadosOriginais *= 10;
        helper_data();
    } else {
        return;
    }


}

TutorialData::~TutorialData() {

}

void TutorialData::helper_data(){
    mPontoMedio = mean(mDadosOriginais);
    mPontoMedio = mPontoMedio.row(0);
    mDadosNormalizados = mDadosOriginais;
    for (int i = 0; i < 3; i++) {
        mDadosNormalizados.col(i) -= mPontoMedio(0, i);
    }

    vec s;
    svd_econ(mU, s, mV, mDadosNormalizados);
    mS = mat(3, 3);
    mS.zeros();
    mS(0, 0) = s(0, 0);
    mS(1, 1) = s(1, 0);
    mS(2, 2) = s(2, 0);

    mAutovetor1 = mV.col(0);
    mAutovetor2 = mV.col(1);
    mAutovetor3 = mV.col(2);

    mat v12;
    v12.insert_cols(0, mAutovetor1);
    v12.insert_cols(1, mAutovetor2);
    mat p12 = v12 * v12.t();
    mMatrizProjecoes.push_back(p12);
    mProjecaoNormalizadaPlano12 = mDadosNormalizados * (p12);
    mProjecoesNormalizadas.push_back(mProjecaoNormalizadaPlano12);

    mProjecaoSemMediaPlano12 = mProjecaoNormalizadaPlano12;
    for (int j = 0; j < 3; j++) {
        mProjecaoSemMediaPlano12.col(j) += mPontoMedio(0, j);
    }
    mProjecoesSemMedia.push_back(mProjecaoSemMediaPlano12);

    mat v23;
    v23.insert_cols(0, mAutovetor2);
    v23.insert_cols(1, mAutovetor3);
    mat p23 = v23 * v23.t();
    mMatrizProjecoes.push_back(p23);
    mProjecaoNormalizadaPlano23 = mDadosNormalizados * (p23);
    mProjecoesNormalizadas.push_back(mProjecaoNormalizadaPlano23);
    mProjecaoSemMediaPlano23 = mProjecaoNormalizadaPlano23;
    for (int j = 0; j < 3; j++) {
        mProjecaoSemMediaPlano23.col(j) += mPontoMedio(0, j);
    }
    mProjecoesSemMedia.push_back(mProjecaoSemMediaPlano23);

    mat v13;
    v13.insert_cols(0, mAutovetor1);
    v13.insert_cols(1, mAutovetor3);
    mat p13 = v13 * v13.t();
    mMatrizProjecoes.push_back(p13);
    mProjecaoNormalizadaPlano13 = mDadosNormalizados * (p13);
    mProjecoesNormalizadas.push_back(mProjecaoNormalizadaPlano13);
    mProjecaoSemMediaPlano13 = mProjecaoNormalizadaPlano13;
    for (int j = 0; j < 3; j++) {
        mProjecaoSemMediaPlano13.col(j) += mPontoMedio(0, j);
    }
    mProjecoesSemMedia.push_back(mProjecaoSemMediaPlano13);
}