#include "tutorialdiretor.hpp"

using namespace std;
using namespace arma;

TutorialDiretor::TutorialDiretor(Cena *cena) {
    mCena = cena;
    mEstadoAtual = DADOS_VAZIO;
    mPlanoAtual = NENHUM;
    mNuvemDadosOriginais = NULL;
    mEsferaPontoMedio = NULL;        

    mCena->add(new Linha(-1, 0, 0, 20, 0, 0));
    mCena->add(new Linha(0, -1, 0, 0, 20, 0));
    mCena->add(new Linha(0, 0, -1, 0, 0, 20));

    QThreadPool::globalInstance()->setMaxThreadCount(100);
}

void TutorialDiretor::iniciar(TutorialData *tutorialData) {
    if (mEstadoAtual == DADOS_VAZIO) {
        mTutorialData = tutorialData;

        removerTodosObjetos3D();
        construirObjetos3D();

        vector<Objeto3D*> aux = mNuvemDadosOriginais->getObjetos();
        for (Objeto3D* obj: aux) {
            FadeInAnimacao *a = new FadeInAnimacao(obj);
            a->setDelay(random() % 1000);
            a->play();
        }
        mEstadoAtual = DADOS_ORIGINAIS;
    } else if (mEstadoAtual == DADOS_ORIGINAIS) {
        vector<Objeto3D*> aux = mNuvemDadosOriginais->getObjetos();
        FadeOutAnimacao *a = new FadeOutAnimacao(mEsferaPontoMedio);
        a->play();
        for (Objeto3D* obj: aux) {
            FadeOutAnimacao *a = new FadeOutAnimacao(obj);
            a->setDelay(random() % 1000);
            a->play();
        }
        mEstadoAtual = DADOS_VAZIO;
    }
}

void TutorialDiretor::exibirCentroide() {
    if (mEstadoAtual != DADOS_ORIGINAIS) {
        return;
    }        
    FadeInAnimacao *aux = new FadeInAnimacao(mEsferaPontoMedio);
    aux->play();
}

void TutorialDiretor::ocultarCentroide() {
    if (mEstadoAtual != DADOS_ORIGINAIS) {
        return;
    }
    FadeOutAnimacao *aux = new FadeOutAnimacao(mEsferaPontoMedio);
    aux->play();        
}

void TutorialDiretor::normalizar() {
    if (mEstadoAtual == DADOS_ORIGINAIS) {
        TransladarAnimacao *aux = new TransladarAnimacao(mEsferaPontoMedio, 0, 0, 0);
        aux->play();
        transladarPontosNuvem(mTutorialData->getDadosNormalizados());
        mEstadoAtual = DADOS_NORMALIZADOS;
    } else if (mEstadoAtual == DADOS_NORMALIZADOS) {
        ocultarPlano();
        rowvec pontoMedio = mTutorialData->getPontoMedio();
        TransladarAnimacao *apm = new TransladarAnimacao(mEsferaPontoMedio, pontoMedio[0], pontoMedio[1], pontoMedio[2]);
        apm->play();

        transladarPontosNuvem(mTutorialData->getDadosOriginais());
        mEstadoAtual = DADOS_ORIGINAIS;
    }
}

void TutorialDiretor::exibirPlano(Plano plano) {
    if (mEstadoAtual != DADOS_NORMALIZADOS) {
        return;
    }

    if (mPlanoAtual != plano) {
        ocultarPlano();
    }

    mPlanoAtual = plano;
    if (mPlanoAtual != NENHUM) {
        Objeto3D *f = mPlanos[static_cast<int>(mPlanoAtual) - 1];
        FadeInAnimacao *aux = new FadeInAnimacao(f);
        aux->play();
    }
}

void TutorialDiretor::ocultarPlano() {
    if (mPlanoAtual != NENHUM) {
        Objeto3D *f = mPlanos[static_cast<int>(mPlanoAtual) - 1];
        FadeOutAnimacao *aux = new FadeOutAnimacao(f);
        aux->play();
    }
}

void TutorialDiretor::projetarPontos() {        
    if (mEstadoAtual == DADOS_NORMALIZADOS && mPlanoAtual != NENHUM) {
        transladarPontosNuvem(mTutorialData->getProjecoesNormalizadas()[((int) mPlanoAtual) - 1]);
        mEstadoAtual = DADOS_NORMALIZADOS_PROJETADOS;
    } else if (mEstadoAtual == DADOS_NORMALIZADOS_PROJETADOS) {
        transladarPontosNuvem(mTutorialData->getDadosNormalizados());
        mEstadoAtual = DADOS_NORMALIZADOS;
    }
}

void TutorialDiretor::transladarPontosNuvem(mat posicoes) {
    for (int i = 0, n = posicoes.n_rows; i < n; i++) {
        Objeto3D * obj = mNuvemDadosOriginais->getObjetos()[i];
        rowvec alvo = posicoes.row(i);
        TransladarAnimacao *aux = new TransladarAnimacao(obj, alvo[0], alvo[1], alvo[2]);
        aux->play();
    }
}

void TutorialDiretor::retirarMedia() {
    if (mEstadoAtual == DADOS_NORMALIZADOS_PROJETADOS) {
        ocultarPlano();
        rowvec pontoMedio = mTutorialData->getPontoMedio();
        TransladarAnimacao *apm = new TransladarAnimacao(mEsferaPontoMedio, pontoMedio[0], pontoMedio[1], pontoMedio[2]);
        apm->play();
        transladarPontosNuvem(mTutorialData->getProjecoesSemMedia()[((int) mPlanoAtual) - 1]);
        mEstadoAtual = DADOS_PROJETADOS_FINAIS;
    } else if (mEstadoAtual == DADOS_PROJETADOS_FINAIS) {
        exibirPlano(mPlanoAtual);
        transladarPontosNuvem(mTutorialData->getProjecoesNormalizadas()[((int) mPlanoAtual) - 1]);
        mEstadoAtual = DADOS_NORMALIZADOS_PROJETADOS;
    }
}

void TutorialDiretor::removerObjeto3D(Objeto3D *o) {
    mCena->remove(o);
    delete o;
}

void TutorialDiretor::removerTodosObjetos3D() {
    removerObjeto3D(mNuvemDadosOriginais);
    removerObjeto3D(mEsferaPontoMedio);        
}

void TutorialDiretor::construirObjetos3D() {
    mNuvemDadosOriginais = criarNuvemPontos(mTutorialData->getDadosOriginais(), Qt::darkRed);        
    mCena->add(mNuvemDadosOriginais);        

    rowvec pontoMedio = mTutorialData->getPontoMedio();
    mEsferaPontoMedio = criarEsfera(pontoMedio, 1, Qt::darkYellow);
    mEsferaPontoMedio->setVisible(false);
    mCena->add(mEsferaPontoMedio);

    mat p12 = mTutorialData->getMatrizProjecoes()[0];
    mat p23 = mTutorialData->getMatrizProjecoes()[1];
    mat p13 = mTutorialData->getMatrizProjecoes()[2];

    Retangulo *aux = criarPlano(normalise(p12.col(0)), normalise(p12.col(1)));
    mPlanos.push_back(aux);
    aux->setColor(QColor(76,158,160));
    mCena->add(aux);
    aux = criarPlano(normalise(p23.col(0)), normalise(p23.col(1)));
    mPlanos.push_back(aux);        
    mCena->add(aux);
    aux = criarPlano(normalise(p13.col(0)), normalise(p13.col(1)));
    mPlanos.push_back(aux);
    mCena->add(aux);
}

Estado TutorialDiretor::getEstadoAtual() {
    return mEstadoAtual;
}

TutorialDiretor::~TutorialDiretor() {
    removerTodosObjetos3D();
}

GrupoObjetos3D *TutorialDiretor::criarNuvemPontos(mat pontos, QColor cor) {
    GrupoObjetos3D *grupo = new GrupoObjetos3D();
    float raio = 0.3;
    for (int i = 0, n = pontos.n_rows; i < n; i++) {
        grupo->add(criarEsfera(pontos.row(i), raio, cor));
    }
    return grupo;
}

Esfera *TutorialDiretor::criarEsfera(rowvec vetor, float raio, QColor cor) {
    Esfera *aux = new Esfera;
    aux->setLayoutX(vetor[0]);
    aux->setLayoutY(vetor[1]);
    aux->setLayoutZ(vetor[2]);
    aux->setRaio(raio);
    aux->setColor(cor);
    return aux;
}

Retangulo *TutorialDiretor::criarPlano(colvec base1, colvec base2) {
    base1 *= 0.5;
    base2 *= 0.5;
    double oX = 0;
    double oY = 0;
    double oZ = 0;
    double aX = oX - base1[0] - base2[0];
    double aY = oY - base1[1] - base2[1];
    double aZ = oZ - base1[2] - base2[2];

    double bX = oX + base1[0] - base2[0];
    double bY = oY + base1[1] - base2[1];
    double bZ = oZ + base1[2] - base2[2];

    double cX = oX + base1[0] + base2[0];
    double cY = oY + base1[1] + base2[1];
    double cZ = oZ + base1[2] + base2[2];

    double dX = oX - base1[0] + base2[0];
    double dY = oY - base1[1] + base2[1];
    double dZ = oZ - base1[2] + base2[2];
    //double deltaX = (max(aX, max(bX, max(cX, dX))) - min(aX, min(bX, min(cX, dX)))) / 2;
    //double deltaY= (max(aY, max(bY, max(cY, dY))) - min(aY, min(bY, min(cY, dY)))) / 2;
    Retangulo *r = new Retangulo(aX, aY, aZ, bX, bY, bZ, cX, cY, cZ, dX, dY, dZ);
    //Retangulo *r = new Retangulo(aX -deltaX, aY-deltaY, aZ, bX-deltaX, bY-deltaY, bZ, cX-deltaX, cY-deltaY, cZ, dX-deltaX, dY-deltaY, dZ);
    r->setColor(QColor(0, 128, 128, 60));
    r->setScaleX(20);
    r->setScaleY(20);
    r->setScaleZ(20);
    r->setVisible(false);
    return r;
}       