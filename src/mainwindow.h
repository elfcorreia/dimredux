#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>
#include <QMainWindow>
#include <QOpenGLWidget>
#include <QVector>
#include "tutorialdata.hpp"
#include "tutorialdiretor.hpp"
#include "selecionardadosdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void on_btnSelecionarDados_clicked();
    void on_btnVoltar_clicked();
    void on_btnExibirOcultarCentroide_clicked();
    void on_btnNormalizarDados_clicked();
    void on_cbxPlanos_currentIndexChanged(int index);
    void on_btnRetirarMedia_clicked();
    void on_btnProjetarPlano_clicked();
    void gerar_vetoresAleatorios(int quantidade, bool aleatorio);
    void gerar_numerosDoArquivo(QVector<float> lst_numeros, bool aleatorio);

private:
    Ui::MainWindow *ui;    

    Cena *mCena;
    TutorialData *mTutorialData;
    TutorialDiretor *mTutorialDiretor;
    SelecionarDadosDialog *dlg;

    bool mExibirOcultar;
    void exibirDados();
    void atualizarBotoes();

};

#endif // MAINWINDOW_H
