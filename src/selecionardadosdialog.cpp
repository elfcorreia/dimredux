#include "selecionardadosdialog.h"
#include "ui_selecionardadosdialog.h"
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>

using namespace std;

SelecionarDadosDialog::SelecionarDadosDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelecionarDadosDialog)
{
    ui->setupUi(this);

    if(ui->rdArquivo->isChecked()){
        ui->nomeArquivo->setEnabled(true);
        ui->btnProcurar->setEnabled(true);
        ui->lblQuantidade->setEnabled(false);
        ui->spnQuantidade->setEnabled(false);
    }
    else {
        ui->nomeArquivo->setEnabled(false);
        ui->btnProcurar->setEnabled(false);
        ui->lblQuantidade->setEnabled(true);
        ui->spnQuantidade->setEnabled(true);
    }
}

SelecionarDadosDialog::~SelecionarDadosDialog()
{
    delete ui;
}

void SelecionarDadosDialog::loadFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return;
    }

    setCurrentFile(fileName);
    lerDados(&file);
    atualizarDialog();

}

void SelecionarDadosDialog::setCurrentFile(const QString &fileName)
{
    curFile = fileName;

}

void SelecionarDadosDialog::on_rdArquivo_clicked(bool checked)
{
    if(checked){
        ui->nomeArquivo->setEnabled(true);
        ui->btnProcurar->setEnabled(true);
        ui->lblQuantidade->setEnabled(false);
        ui->spnQuantidade->setEnabled(false);
    }
    else {
        ui->nomeArquivo->setEnabled(false);
        ui->btnProcurar->setEnabled(false);
        ui->lblQuantidade->setEnabled(true);
        ui->spnQuantidade->setEnabled(true);
    }
}

void SelecionarDadosDialog::on_rdAleatorio_clicked(bool checked)
{
    if(checked){
        ui->lblQuantidade->setEnabled(true);
        ui->spnQuantidade->setEnabled(true);
        ui->nomeArquivo->setEnabled(false);
        ui->btnProcurar->setEnabled(false);

    }
    else{
        ui->lblQuantidade->setEnabled(false);
        ui->spnQuantidade->setEnabled(false);
        ui->nomeArquivo->setEnabled(true);
        ui->btnProcurar->setEnabled(true);

    }
}

void SelecionarDadosDialog::on_btnProcurar_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this);
    if (!fileName.isEmpty()){
        loadFile(fileName);
    }

}

void SelecionarDadosDialog::on_pushButton_clicked()
{
    if(ui->rdAleatorio->isChecked()){
        bool aleatorio = true;
        int quantidade = ui->spnQuantidade->value();
        emit quantidadeDialog(quantidade, aleatorio);
    }
    else{
        bool aleatorio = false;
        emit listaNumerosDialog(lst_numValor, aleatorio);
    }
}

void SelecionarDadosDialog::atualizarDialog()
{
    ui->nomeArquivo->setText(curFile);
}

void SelecionarDadosDialog::lerDados(QFile *file)
{
    QTextStream in(file);
    QString str = in.readAll();
    lst_strValor = str.split(QRegExp("\\s+"));

    int count = lst_strValor.count();
    if( (count-1)%3 == 0){
        for(int i = 0; i < count; i++){
            float x = lst_strValor.value(i).toFloat();
            lst_numValor.append(x);
        }
    }
}
