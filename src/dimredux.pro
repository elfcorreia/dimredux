#-------------------------------------------------
#
# Project created by QtCreator 2015-01-21T15:19:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dimredux
TEMPLATE = app

SOURCES += animacao.cpp\ 
	cena.cpp\ 
	esfera.cpp\ 
	fadeinanimacao.cpp\ 
	grupoanimacao.cpp\ 
	grupoobjetos3d.cpp\ 
	linha.cpp\ 
	main.cpp\ 
	mainwindow.cpp\ 
	model.cpp\ 
	objeto3d.cpp\ 
	planocartesiano.cpp\ 
	retangulo.cpp\ 
	selecionardadosdialog.cpp\ 
	tutorialdata.cpp\ 
	tutorialdiretor.cpp

HEADERS += animacao.h\ 
	cena.h\ 
	esfera.h\ 
	fadeinanimacao.h\ 
	grupoanimacao.h\ 
	grupoobjetos3d.h\ 
	lang.h\ 
	linha.h\ 
	mainwindow.h\ 
	model.h\ 
	objeto3d.h\ 
	planocartesiano.h\ 
	retangulo.h\ 
	selecionardadosdialog.h\ 
	tutorialdata.h\ 
        tutorialdiretor.h

FORMS    += mainwindow.ui \
    selecionardadosdialog.ui

LIBS += -llapack -lblas -larmadillo
