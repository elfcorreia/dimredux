#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <sstream>
#include <iostream>
#include <QThread>
#include "selecionardadosdialog.h"
#include "scene3d/planocartesiano.hpp"
#include "scene3d/cena.hpp"
#include "scene3d/objects/esfera.hpp"
#include "scene3d/objects/linha.hpp"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{    
    ui->setupUi(this);

    dlg = new SelecionarDadosDialog(this);
    connect(dlg, SIGNAL(quantidadeDialog(int,bool)), this, SLOT(gerar_vetoresAleatorios(int,bool)));
    connect(dlg, SIGNAL(listaNumerosDialog(QVector<float>,bool)), this, SLOT(gerar_numerosDoArquivo(QVector<float>,bool)));

    mExibirOcultar = false;
    mTutorialData = new TutorialData;       
    mCena = new Cena;
    std::cout << "after cena creation" << std::endl;
    mTutorialDiretor = new TutorialDiretor(mCena);

    QHBoxLayout *aux = new QHBoxLayout;
    aux->addWidget(mCena);
    ui->tabVisualizacao->setLayout(aux);

    ui->cbxPlanos->addItem(QString("Nenhum"));
    ui->cbxPlanos->addItem(QString("Plano A"));
    ui->cbxPlanos->addItem(QString("Plano B"));
    ui->cbxPlanos->addItem(QString("Plano C"));

    ui->btnExibirOcultarCentroide->setVisible(false);
    ui->btnNormalizarDados->setVisible(false);
    ui->cbxPlanos->setVisible(false);
    ui->btnProjetarPlano->setVisible(false);
    ui->btnRetirarMedia->setVisible(false);
    //ui->btnVoltar->setVisible(false);

    mCena->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::exibirDados()
{    
    stringstream ss;
//   "Dados originais"
    ss << mTutorialData->getDadosOriginais() << endl;
    QString aux = ss.str().c_str();
    ui->dadoOriginais->setPlainText(aux);

    stringstream ss2;
//  "Ponto médio"
    ss2 << mTutorialData->getPontoMedio() << endl;
    aux = ss2.str().c_str();
    ui->pontoMedio->setPlainText(aux);

    stringstream ss3;
//  "Dados normalizados"
    ss3 << mTutorialData->getDadosNormalizados() << endl;
    aux = ss3.str().c_str();
    ui->dadosNormalizados->setPlainText(aux);

    stringstream ss4;
    // Matriz S
    ss4 << mTutorialData->getS() << endl;
    aux = ss4.str().c_str();
    ui->matrizS->setPlainText(aux);

    stringstream ss5;
    // Matriz U (Mas não deveria ser a D?)
    ss5 << mTutorialData->getU() << endl;
    aux = ss5.str().c_str();
    ui->matrizD->setPlainText(aux);

    stringstream ss6;
    // Matriz V
    ss6 << mTutorialData->getV() << endl;
    aux = ss6.str().c_str();
    ui->matrizV->setPlainText(aux);

}

void MainWindow::on_btnSelecionarDados_clicked()
{
    dlg->setModal(true);
    dlg->show();
}

void MainWindow::on_btnVoltar_clicked()
{   
    switch (mTutorialDiretor->getEstadoAtual()) {
    case DADOS_ORIGINAIS: {
        mTutorialDiretor->iniciar(mTutorialData); // funciona mas precisa ser refatorado :P
        on_btnSelecionarDados_clicked();
        break;
    }
    case DADOS_NORMALIZADOS: {
        on_btnNormalizarDados_clicked();
        break;
    }
    case DADOS_NORMALIZADOS_PROJETADOS: {
        on_btnProjetarPlano_clicked();
        break;
    }
    case DADOS_PROJETADOS_FINAIS: {
        on_btnRetirarMedia_clicked();
        break;
    }
    }
}

void MainWindow::atualizarBotoes() {
    Estado aux = mTutorialDiretor->getEstadoAtual();
    //ui->btnVoltar->setVisible(aux != DADOS_VAZIO);
    ui->btnSelecionarDados->setVisible(aux == DADOS_VAZIO);
    ui->btnExibirOcultarCentroide->setVisible(aux == DADOS_ORIGINAIS);
    ui->btnNormalizarDados->setVisible(aux == DADOS_ORIGINAIS);
    ui->btnProjetarPlano->setVisible(aux == DADOS_NORMALIZADOS);
    ui->cbxPlanos->setVisible(aux == DADOS_NORMALIZADOS);    
    ui->btnRetirarMedia->setVisible(aux == DADOS_NORMALIZADOS_PROJETADOS);
}


void MainWindow::on_btnExibirOcultarCentroide_clicked()
{
    if (mExibirOcultar) {
        mTutorialDiretor->ocultarCentroide();
    } else {
        mTutorialDiretor->exibirCentroide();
    }
    mExibirOcultar = !mExibirOcultar;
}


void MainWindow::on_btnNormalizarDados_clicked()
{    
    mTutorialDiretor->normalizar();
    atualizarBotoes();
}

void MainWindow::on_cbxPlanos_currentIndexChanged(int index)
{
    mTutorialDiretor->exibirPlano((Plano) index);
}

void MainWindow::on_btnRetirarMedia_clicked()
{
    mTutorialDiretor->retirarMedia();
    atualizarBotoes();
}

void MainWindow::on_btnProjetarPlano_clicked()
{
    mTutorialDiretor->projetarPontos();
    atualizarBotoes();
}

void MainWindow::gerar_vetoresAleatorios(int quantidade, bool aleatorio)
{
    mTutorialData = new TutorialData(quantidade, aleatorio);
    mTutorialDiretor->iniciar(mTutorialData);
    exibirDados();
    atualizarBotoes();
}

void MainWindow::gerar_numerosDoArquivo(QVector<float> lst_numeros, bool aleatorio)
{
    qDebug() << "m passo 0";
    mTutorialData = new TutorialData(lst_numeros, aleatorio);
    mTutorialDiretor->iniciar(mTutorialData);
    exibirDados();
    atualizarBotoes();
}
