#ifndef TUTORIALDIRETOR_H
#define TUTORIALDIRETOR_H

#include <vector>
#include <Qt>
#include <armadillo>
#include <iostream>
#include "scene3d/cena.hpp"
#include "scene3d/objects/grupoobjetos3d.hpp"
#include "scene3d/objects/esfera.hpp"
#include "scene3d/objects/linha.hpp"
#include "scene3d/objects/retangulo.hpp"
#include "scene3d/animations/fadeinanimacao.hpp"
#include "scene3d/animations/fadeoutanimacao.hpp"
#include "scene3d/animations/transladaranimacao.hpp"
#include "tutorialdata.hpp"

enum Estado {
    DADOS_VAZIO,
    DADOS_ORIGINAIS,
    DADOS_NORMALIZADOS,
    DADOS_NORMALIZADOS_PROJETADOS,
    DADOS_PROJETADOS_FINAIS
};

enum Plano {
    NENHUM,
    P12,
    P23,
    P13
};

class TutorialDiretor {    
private:
    TutorialData *mTutorialData;
    Estado mEstadoAtual;
    Cena *mCena;

    Plano mPlanoAtual;    
    GrupoObjetos3D* mNuvemDadosOriginais;
    Esfera* mEsferaPontoMedio;
    std::vector<Retangulo*> mPlanos;
    
    void ocultarPlano();
    inline void removerObjeto3D(Objeto3D *o);
    GrupoObjetos3D *criarNuvemPontos(arma::mat pontos, QColor cor);
    Esfera *criarEsfera(arma::rowvec vetor, float raio, QColor cor);
    Retangulo *criarPlano(arma::colvec base1, arma::colvec base2);
    void transladarPontosNuvem(arma::mat posicoes);
    void removerTodosObjetos3D();
    void construirObjetos3D();
public:
    TutorialDiretor(Cena *cena);
    ~TutorialDiretor();

    void iniciar(TutorialData *tutorialData);
    void exibirCentroide();
    void ocultarCentroide();
    void normalizar();
    void exibirPlano(Plano plano);
    void projetarPontos();
    void retirarMedia();
    Estado getEstadoAtual();
};

#endif // TUTORIALDIRETOR_H
