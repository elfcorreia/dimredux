# Dimredux

## What is it?

A Qt5/OpenGL/Armadillo Tutorial for dimensionality reduction.

Step-to-step dimension reducer algorithm using SVD decomposition over the covariance matrix of a 3D point dataset.

## Dependencies

 * Qt 5.4
 * Armadillo 9.0
 * OpenGL 3.3

## How to compile
	
Inside the dimredux dir, run:

```
mkdir build && cd build
cmake ..
make
```